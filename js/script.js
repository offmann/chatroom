
template_message =   '<li class="media">\
<div class="media-body"> <div class="media">\
 <a class="pull-left" href="#">\
 <img class="media-object img-circle " style="max-height:50px; max-width:50px;"  src="#url_img#" />  \
                                                </a>  \
                                                <div class="media-body" >  \
													#message#\
                                                    <br />  \
                                                   <small class="text-muted">#name#| #date#</small>  \
                                                    <hr />  \
                                                </div>  \
                                            </div>  \
                                        </div>  \
                                    </li>'


template_user =   '<li class="media">\
<div class="media-body"> <div class="media">\
<img class="media-object img-circle" style="max-height:40px; max-width:40px;" src="#url_img#" />\
		</a>\
		<div class="media-body" >\
			<h5> #username# </h5>\
		   <small class="text-muted"> Dernière apparition : #date_envoi# </small>\
		</div>\
	</div>\
</div>\
</li>'                   


// Traitements sur les messages 

	// appel Ajax pour récupérer la réponse json de messages.php
	function get_message(){
		var xhr = new XMLHttpRequest();
		xhr.open('GET','/chatroomwa/php/messages.php');
		xhr.addEventListener('load',function(){
			var response = this.response;
			console.log(response);
			obj = JSON.parse(response); 
			display_messages(obj);
		});
	xhr.send();	
	}

	// création dynamique du contenu HTML 
	function display_messages(messages_list)
	{
		var message_holder = document.getElementById("messages_holder");
		messages_list.forEach(function(el){
			var message = template_message.replace("#message#", el["message"])
			message = message.replace("#name#", el["username"]);
			message = message.replace("#date#", el["date_envoi"]);
			//message = message.replace("#date#", el["date_envoi"]);
			message = message.replace("#url_img#", el["url_img"]);	
			message_holder.innerHTML += message;
		});
	}
	get_message();


	// appel ajax pour insérer un nouveau message
	function insert_messages()
	{
		var xhttp = new XMLHttpRequest();
		xhttp.open('POST','/chatroomwa/php/insert_messages.php',true);				
		xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhttp.addEventListener('readystatechange',function()
		{
			if (xhttp.readyState === XMLHttpRequest.DONE)
			{
								
				var message_holder = document.getElementById("messages_holder");
				message = template_message.replace("#message#", document.getElementById("message").value);
				message = template_message.replace("#username#", document.getElementById("username").value);
				message_holder.innerHTML += message;
		
			}
		});					
		var message = document.getElementById("message").value;
		xhttp.send("message="+message);
	}


		
// Traitements sur les users dernièrement en activité


	function get_users(){
		var xhr = new XMLHttpRequest();
		xhr.open('GET','/chatroomwa/php/connected_users.php');
		xhr.addEventListener('load',function(){
			var response = this.response;
			console.log(response);
			obj = JSON.parse(response); 
			display_users(obj);
		});
	xhr.send();	
	}

	function display_users(users_list)
	{
		var users_holder = document.getElementById("users_holder");
		users_list.forEach(function(el){
			var user = template_user.replace("#username#", el["username"])
			user = user.replace("#date_envoi#", el["date_envoi"]);
			user = user.replace("#url_img#",el["url_img"]);
			users_holder.innerHTML += user;
		});
	}
	get_users();


