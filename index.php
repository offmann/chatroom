<?php include_once("./php/sessions.php");
ob_start(); // pour ne pas charger la page avant le traitement dans online.php // header redirection si l'utilisateur clique sur logout
?>
<!--
All eode is under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007.
-->
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<head>
<!-- Import Bootstrap from CDN-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--Extra Theme-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!--Import jQuary from CDN-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Extra CSS -->
<style>
.text-right {
  float: right;
}
body {
  background: #d4eee9;
}
</style>
</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php"> MIASHS Chatroom Project </a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php"> Accueil </a></li>
      <?php if(isset($_SESSION["username"])){
        echo "
            <li class='active'><a href='./html/display_messages.php'> Messages </a></li>
            </ul> ";}
            ?>
    <ul class="nav navbar-nav navbar-right">
      <?php
        if(isset($_SESSION["username"])){
           include_once("./php/online.php");
        }
       ?>
    </ul>
  </div>
</nav>
  <div class="row">
    <div class="col-md-9">
      <div class="panel panel-primary"> 
	     <div class="panel-heading"> Bienvenue !</div>
		 <div class="panel-body">
        Bienvenue au projet académique du Master MIASHS Parcours Web Analyst.
        Cette plateforme a pour objectif de mettre à disposition un outil de messagerie instantanée.
        <a target="_blank" rel="noopener noreferrer" href="https://paperman.name/#ens./teaching/2018/web_analyst/0"><font color="Red">Cahier de charges</font></a>
		 </div>
      </div>
    </div>
	<div class="col-md-3">
	  <div class="panel panel-primary"> 
    <?php if(!isset($_SESSION["username"])){ 
          echo "  <div class='panel-heading'>Notes</div>
	                <div class='panel-body'>
                  <a href='html/login.html'><font color='red'>Connectez-vous</font></a> pour discuter avec la communauté. 
                  Si vous n'êtes pas membre, <a href='html/login.html'><font color='red'>Inscrivez-vous!</font></a> 
	                </div>" ;} ?>
                  </div>
   </div>
  </div>
<!-- Copyright & Credits bar-->
<div class="panel panel-primary">
<div class="panel-heading"> Université de Lille ~ UFR MIME .<span class="text-right"> HTML/CSS template by <a href="https://github.com/JoeZwet"><font color="black">Joe Zwet</font></a></span></div>
</div>
</div>
</body>
</html>
