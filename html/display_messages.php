<?php include_once '../php/sessions.php' ;
ob_start(); // pour ne pas charger la page avant le traitement dans online.php header redirection si l'utilisateur clique sur logout
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
    

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--Extra Theme-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title> MIASHS Chatroom Project </title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="msg.css" rel="stylesheet" />

    <style>
            .text-right {
              float: right;
            }
            body {
              background: #d4eee9;
            }
            </style>

</head>
<body style="font-family:Verdana">
  <div class="container">
        <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <a class="navbar-brand" href="#"> MIASHS Chatroom Project </a>
                  </div>
                  <ul class="nav navbar-nav">
                    <li><a href="../index.php"> Accueil </a></li>
                    <li class="active"><a href="#"> Messages </a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                    <?php include_once("../php/online.php");?>
                     </ul>
                </div>
              </nav>
 

<div class="row " style="padding-top:40px;">
    <h3 class="text-center" > Bienvenue </h3>
    <br /><br />
    <div class="col-md-8">
        <div class="panel panel-info">
            <div class="panel-heading">
                Messages récents
			</div>
			
            <div class="panel-body">
				<ul class="media-list" id="messages_holder"> </ul>
			</div>

            <div class="panel-footer">
                <div class="input-group">
                                    <input type="text" class="form-control" id = "message" name = "message" placeholder="Enter Message" />
                                    <span class="input-group-btn">
                                        <button id = "envoyer" class="btn btn-info" type="submit" name = "message" onclick="insert_messages()" > Envoyer </button>
                                    </span>
                                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
               Utilisateurs récemment actifs
			</div>

			<div class="panel-body">
				<ul class="media-list" id="users_holder"> </ul>
			</div>

</div>
  </div>
</div>  
</div>  

<script src='/chatroomwa/js/script.js'> </script>


</body>
</html>
