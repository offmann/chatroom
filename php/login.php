<?php
include_once("sessions.php");
include '../connection.php';

if (isset($_POST['username']) AND isset($_POST['password'])) // Pour tester si les valeurs ont été remplies


{
$query = $db->prepare("SELECT * FROM users WHERE 
    username = :username AND motdepasse = :password");
    
    $query->bindParam(':username', $_POST['username']);
    $query->bindParam(':password', $_POST['password']);
    $query->execute();
    
$count = $query->rowCount();

$row=$query->fetch(PDO::FETCH_ASSOC);

if($count==1){
    //session_start();
    $_SESSION['logged']=true;
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['user_id']= $row['user_id'];
    $_SESSION['url_img']= $row['url_img'];
    header("Location: ../html/display_messages.php"); // afficher la page contenant le username de la session
    exit();
}
else {
    $_SESSION['logged']=false;
    header("Location: ../html/login.html");
}
  
} 
  
?>

