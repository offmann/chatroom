<?php

include '../connection.php';

	$username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $pass = !empty($_POST['password']) ? trim($_POST['password']) : null;
    $sql = "SELECT COUNT(username) AS num FROM users WHERE username = :username";
    $stmt = $db->prepare($sql);
    

    $stmt->bindValue(':username', $username);
    
    $stmt->execute();
    
    //Fetch the row.
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($row['num'] > 0){
        die('Login déjà existant! Veuillez en choisir un autre');
    }

    else {
		$sql = "INSERT INTO users (username, motdepasse) VALUES (:username, :password)";
		$stmt = $db->prepare($sql);
		
		$stmt->bindValue(':username', $username);
		$stmt->bindValue(':password', $pass);
	 

		$result = $stmt->execute();
		

		if($result){
            header('Location: ../html/login.html');
			//echo "Merci de s'être enregistré";
		}
    
	}


?>